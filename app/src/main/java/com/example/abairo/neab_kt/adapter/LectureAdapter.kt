package com.example.abairo.neab_kt.adapter

import android.content.Context
import android.content.Intent

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import com.bumptech.glide.Glide
import com.example.abairo.neab_kt.DetailLectureActivity
import com.example.abairo.neab_kt.R
import com.example.abairo.neab_kt.model.Lecture
import kotlinx.android.synthetic.main.news_layout.view.*


class LectureAdapter(internal var context: Context, internal var lectureList: List<Lecture>)
    : RecyclerView.Adapter<LectureViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LectureViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.lecture_layout, parent, false)
        return LectureViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return lectureList.size
    }

    override fun onBindViewHolder(holder: LectureViewHolder, position: Int) {
        holder.lecture_title.text = lectureList[position].title
//        holder.news_description.text = StringBuilder(newsList[position].newsDescription.substring(0, 20))
//                .append("...").toString()
        holder.lecture_description.text = lectureList[position].description
        holder.lecture_content.text = lectureList[position].content
        Glide.with(context).load(lectureList[position].image).into(holder.lecture_image);
        holder.itemView.setOnClickListener{
            val html = it.txtContent.text
            //val html = "<p><img alt=\"\" src=\"/media/uploads/2018/06/11/31404076_980863665423702_1986553456801873920_o.jpg\" style=\"height:76px; width:200px\" /></p>"
            val it = Intent(context, DetailLectureActivity::class.java)
            it.putExtra("html_content", html)
            context.startActivity(it)
            
        }
    }

}