package com.example.abairo.neab_kt

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.abairo.neab_kt.model.ContactUs
import com.example.abairo.neab_kt.retrofit.IMyAPI
import com.example.abairo.neab_kt.retrofit.RetrofitClient

import kotlinx.android.synthetic.main.activity_help.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



class HelpActivity : AppCompatActivity() {
    internal lateinit var jsonApi: IMyAPI

    fun clearHelpForm() {
        contactName.text.clear()
        contactEmail.text.clear()
        contactPhone.text.clear()
        contactDescription.text.clear()
    }

    fun validationForm(): Boolean {
        if (contactName.text.length < 1) {
            Toast.makeText(this@HelpActivity, "Campo \"Nome\" inválido", Toast.LENGTH_SHORT).show()
            contactName.requestFocus()
            return false
        }
        if (contactEmail.text.length < 1) {
            Toast.makeText(this@HelpActivity, "Campo \"Email\" inválido", Toast.LENGTH_SHORT).show()
            contactEmail.requestFocus()
            return false
        }
        if (contactPhone.text.length < 1) {
            Toast.makeText(this@HelpActivity, "Campo \"Telefone\" inválido", Toast.LENGTH_SHORT).show()
            contactPhone.requestFocus()
            return false
        }
        if (contactDescription.text.length < 1) {
            Toast.makeText(this@HelpActivity, "Campo \"Descrição\" inválido", Toast.LENGTH_SHORT).show()
            contactDescription.requestFocus()
            return false
        }

        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        btnSendContact.setOnClickListener({
            if(validationForm()){
                val retrofit = RetrofitClient.instance
                jsonApi = retrofit.create(IMyAPI::class.java)
                val contact = ContactUs()

                contact.name = contactName.text.toString()
                contact.email = contactEmail.text.toString()
                contact.phone = contactPhone.text.toString()
                contact.description = contactDescription.text.toString()

                val call = jsonApi.createContactUs(contact)
                call.enqueue(object : Callback<ContactUs> {
                    override fun onFailure(call: Call<ContactUs>?, t: Throwable?) {
                        val a = 1
                    }

                    override fun onResponse(call: Call<ContactUs>?, response: Response<ContactUs>?) {
                        if (response != null && response.isSuccessful) {
                            Toast.makeText(this@HelpActivity,  "Mensagem registrada com sucesso!", Toast.LENGTH_LONG).show()
                            clearHelpForm()
                        } else {
                            Toast.makeText(this@HelpActivity,  "Ocorreu um erro, por favor tente mais tarde.", Toast.LENGTH_LONG).show()
                        }
                    }

                })
            }
        })

    }
}
