package com.example.abairo.neab_kt
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import kotlinx.android.synthetic.main.activity_detail_news.*


class DetailNews : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_news)
        val html_string = intent.getStringExtra("html_content")
        webView.loadData(html_string, "text/html", "utf-8")

        val toolbar = this.findViewById(R.id.action_bar) as Toolbar
        if (toolbar != null) {
            toolbar!!.setTitleTextColor(Color.parseColor("#ffb800"))
        }
    }

}
