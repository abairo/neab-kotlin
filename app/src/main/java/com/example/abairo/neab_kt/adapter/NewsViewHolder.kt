package com.example.abairo.neab_kt.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.news_layout.view.*

class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val news_title = itemView.txtTitle
    val news_description = itemView.txtDescription
    val news_image = itemView.imageNews
    val news_content = itemView.txtContent
}
