package com.example.abairo.neab_kt

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.abairo.neab_kt.adapter.LectureAdapter
import com.example.abairo.neab_kt.model.Lecture
import com.example.abairo.neab_kt.retrofit.IMyAPI
import com.example.abairo.neab_kt.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.lecture_recycler.*


class LectureListFragment : Fragment() {
    internal lateinit var jsonApi: IMyAPI
    var compositeDisposable= CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //Init API
        val retrofit = RetrofitClient.instance
        jsonApi = retrofit.create(IMyAPI::class.java)

        return LayoutInflater.from(container?.context).inflate(R.layout.lecture_recycler, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        //View
        recycler_lecture.setHasFixedSize(true)
        recycler_lecture.layoutManager = LinearLayoutManager(context)
        fetchData()
        super.onActivityCreated(savedInstanceState)
    }

    private fun fetchData() {
        compositeDisposable.add(jsonApi.lecture
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{lecture->displayData(lecture)}
        )
    }

    private fun displayData(lecture: List<Lecture>?) {

        val adapter = LectureAdapter(context!!, lecture!!)
        recycler_lecture.adapter = adapter
    }
}