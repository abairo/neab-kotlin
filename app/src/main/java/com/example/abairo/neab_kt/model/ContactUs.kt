package com.example.abairo.neab_kt.model


public class ContactUs {
    var name: String = ""
    var email: String = ""
    var phone: String = ""
    var description: String = ""
}