package com.example.abairo.neab_kt

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.abairo.neab_kt.retrofit.IMyAPI
import com.example.abairo.neab_kt.retrofit.RetrofitClient
import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    internal lateinit var jsonApi: IMyAPI
    var compositeDisposable= CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.setTitleTextColor(Color.parseColor("#ffb800"))
        toolbar.setTitle("NEAB - Notícias")
        FirebaseMessaging.getInstance().isAutoInitEnabled()
        //Init API
        val retrofit = RetrofitClient.instance
        jsonApi = retrofit.create(IMyAPI::class.java)

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        toolbar.setTitle("NEAB - Notícias")
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment_content, NewsListFragment())
        ft.commit()

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_news -> {
                toolbar.setTitle("NEAB - Notícias")
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_content, NewsListFragment())
                ft.commit()

            }
            R.id.nav_lecture -> {
                toolbar.setTitle("NEAB - Palestras")
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_content, LectureListFragment())
                ft.commit()
            }

            R.id.nav_about -> {
                toolbar.setTitle("NEAB - Sobre")
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_content, AboutFragment())
                ft.commit()
            }

            R.id.nav_initial_page -> {
                toolbar.setTitle("NEAB - Página inicial")
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_content, InitialPageFragment())
                ft.commit()
            }

            R.id.nav_help -> {
                val it = Intent(this, HelpActivity::class.java)
                startActivity(it)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
