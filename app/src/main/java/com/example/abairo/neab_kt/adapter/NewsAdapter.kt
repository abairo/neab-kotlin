package com.example.abairo.neab_kt.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.abairo.neab_kt.DetailNews
import com.example.abairo.neab_kt.NewsListFragment
import com.example.abairo.neab_kt.R
import com.example.abairo.neab_kt.model.News
import kotlinx.android.synthetic.main.news_layout.view.*


class NewsAdapter(internal var context: Context, internal var newsList: List<News>)
    : RecyclerView.Adapter<NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.news_layout, parent, false)
        return NewsViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.news_title.text = newsList[position].title
//        holder.news_description.text = StringBuilder(newsList[position].newsDescription.substring(0, 20))
//                .append("...").toString()
        holder.news_description.text = newsList[position].description
        holder.news_content.text = newsList[position].content
        Glide.with(context).load(newsList[position].image).into(holder.news_image);
        holder.itemView.setOnClickListener{
            val html = it.txtContent.text
            //val html = "<p><img alt=\"\" src=\"/media/uploads/2018/06/11/31404076_980863665423702_1986553456801873920_o.jpg\" style=\"height:76px; width:200px\" /></p>"
            val it = Intent(context, DetailNews::class.java)
            it.putExtra("html_content", html)
            context.startActivity(it)
            
        }
    }

}