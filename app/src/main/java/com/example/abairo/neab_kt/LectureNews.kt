package com.example.abairo.neab_kt
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail_news.*


class LectureNews : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_lecture)
        val html_string = intent.getStringExtra("html_content")
        webView.loadData(html_string, "text/html", "utf-8")
    }

}
