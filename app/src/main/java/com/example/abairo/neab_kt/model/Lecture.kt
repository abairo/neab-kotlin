package com.example.abairo.neab_kt.model

public class Lecture {
    var id: Int = 0
    var title: String = ""
    var description: String = ""
    var image: String = ""
    var content: String = ""
}