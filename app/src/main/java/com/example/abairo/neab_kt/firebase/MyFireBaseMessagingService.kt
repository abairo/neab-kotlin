package com.example.abairo.neab_kt.firebase

import android.util.Log
import android.widget.Toast
import com.example.abairo.neab_kt.model.Device
import com.example.abairo.neab_kt.retrofit.IMyAPI
import com.example.abairo.neab_kt.retrofit.RetrofitClient
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


const val TAG = "MyFirebaseToken"


class MyFireBaseMessagingService: FirebaseMessagingService() {
    internal lateinit var jsonApi: IMyAPI

    override fun onNewToken(p0: String?) {
        Log.d(TAG, " Refreshed token: $p0")

        val retrofit = RetrofitClient.instance
        jsonApi = retrofit.create(IMyAPI::class.java)

        val device = Device()

        device.name = UUID.randomUUID().toString()
        device.registration_id = p0.toString()
        device.type = "android"

        val call = jsonApi.registerDevice(device)

        call.enqueue(object: Callback<Device> {
            override fun onFailure(call: Call<Device>, t: Throwable) {
                Toast.makeText(this@MyFireBaseMessagingService, "Ocorreu um erro ao registrar o dispositivo.", Toast.LENGTH_LONG)
            }

            override fun onResponse(call: Call<Device>, response: Response<Device>) {
                Toast.makeText(this@MyFireBaseMessagingService, "Dispositivo registrado com sucesso.", Toast.LENGTH_LONG)
            }
        })

    }

    override fun onMessageReceived(p0: RemoteMessage?) {
        if(p0?.data != null) {
            Log.d(TAG, " Data: " + p0.data!!.toString())
        }

        if(p0?.notification != null) {
            Log.d(TAG, " Data: " + p0.notification!!.toString())
        }
        super.onMessageReceived(p0)
    }
}