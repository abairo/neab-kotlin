package com.example.abairo.neab_kt

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.abairo.neab_kt.adapter.NewsAdapter
import com.example.abairo.neab_kt.model.News
import com.example.abairo.neab_kt.retrofit.IMyAPI
import com.example.abairo.neab_kt.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_main.*


class NewsListFragment : Fragment() {
    internal lateinit var jsonApi: IMyAPI
    var compositeDisposable= CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //Init API
        val retrofit = RetrofitClient.instance
        jsonApi = retrofit.create(IMyAPI::class.java)

        return LayoutInflater.from(container?.context).inflate(R.layout.content_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        //View
        recycler_news.setHasFixedSize(true)
        recycler_news.layoutManager = LinearLayoutManager(context)
        fetchData()
        super.onActivityCreated(savedInstanceState)
    }

    private fun fetchData() {
        compositeDisposable.add(jsonApi.news
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{news->displayData(news)}
        )
    }

    private fun displayData(news: List<News>?) {

        val adapter = NewsAdapter(context!!, news!!)
        recycler_news.adapter = adapter
    }
}