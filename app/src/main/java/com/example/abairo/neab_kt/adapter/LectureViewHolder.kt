package com.example.abairo.neab_kt.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.lecture_layout.view.*


class LectureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val lecture_title = itemView.txtTitle
    val lecture_description = itemView.txtDescription
    val lecture_image = itemView.imageLecture
    val lecture_content = itemView.txtContent
}
