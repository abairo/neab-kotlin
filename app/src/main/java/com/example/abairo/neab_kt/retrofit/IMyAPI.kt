package com.example.abairo.neab_kt.retrofit

import com.example.abairo.neab_kt.model.*
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface IMyAPI {
    @get:GET("/v1/content/news")
    val news: Observable<List<News>>
    @get:GET("/v1/content/lecture")
    val lecture: Observable<List<Lecture>>
    @POST("v1/communication/contact_us/")
    fun createContactUs(@Body contact: ContactUs): Call<ContactUs>;
    @POST("devices/")
    fun registerDevice(@Body device: Device): Call<Device>;
    @GET("/v1/content/about/")
    fun getAbout(): Call<List<About>>
    @GET("/v1/content/initial_page/")
    fun getInitialPage(): Call<List<InitialPage>>
}