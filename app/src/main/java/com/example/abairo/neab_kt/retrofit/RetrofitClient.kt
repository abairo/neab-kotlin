package com.example.abairo.neab_kt.retrofit

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
// .baseUrl("http://bairo.pythonanywhere.com/")
// .baseUrl("http://192.168.0.128:8000/")
object RetrofitClient {
    private var ourInstance: Retrofit?=null
    val instance:Retrofit
        get() {
            if (ourInstance == null) {
                ourInstance = Retrofit.Builder()
                        .baseUrl("http://bairo.pythonanywhere.com/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
            }
            return ourInstance!!
        }
}